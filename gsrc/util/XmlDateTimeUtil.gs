package util

uses java.util.GregorianCalendar
uses gw.xml.date.XmlDateTime
uses javax.xml.datatype.DatatypeFactory
uses java.util.Date
uses java.util.TimeZone

/**
 * User: Abhijtih
 * Date: 19/04/16
 * Time: 10:29
 * To change this template use File | Settings | File Templates.
 */
class XmlDateTimeUtil {

  static function getXmlDateTime(date : java.util.Date) : XmlDateTime {
    if(date == null) { return null }
    var calendar = new GregorianCalendar();
    calendar.setTime(date);
    var df = DatatypeFactory.newInstance();
    var dateTime = df.newXMLGregorianCalendar(calendar);
    return new XmlDateTime(dateTime.toString())
  }

  static function getDateTime(date : XmlDateTime) : Date {

    if (date == null) {
      return null
    }

    return date.toCalendar(TimeZone.Default).Time
  }


}

package util


uses java.util.Map

uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses gw.plugin.InitializablePlugin
uses robinson.batch.DCNMBatch
uses robinson.batch.RobinsonMailListBatch

@Export
class ProcessPlugin implements InitializablePlugin, IProcessesPlugin {

 private var _pParamMap : Map<Object, Object>

  construct() {
  }

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {

      case BatchProcessType.TC_DCNMIMPORTBATCH:
          return new DCNMBatch()

      case BatchProcessType.TC_ROBINSONSIMPORTBATCH:
          return new RobinsonMailListBatch()

        default:
        return null//super.createBatchProcess(type, arguments)
    }
  }

 override function setParameters(p0 : Map<Object, Object>) {

    _pParamMap = p0

  }

}

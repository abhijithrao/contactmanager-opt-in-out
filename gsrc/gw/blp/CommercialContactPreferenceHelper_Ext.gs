package gw.blp

/**
 * This is a helper class to  Commercial Contact Preference PCF files.
 * @author Rajendra Kumar
 * @see BLT-47 and BLT-64
 */
class CommercialContactPreferenceHelper_Ext {
  /**
   * Updates contact preference fields('Last Update Date Time' and 'Last Source of Update') of digital, phone or paper mail, whenever there is a change in their respective
   * 'opt' option.
   */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Param("code", "a unique code to identify the particular contact preference")
  static function updateContactPreferenceFields(contact:ABContact, code: String){

    switch(code) {
      case "Digital":
          contact.DigitalContactPreference_Ext.isFieldChanged("Opt") {
        contact.DigitalContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
        contact.DigitalContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_INTERNAL
      }
          break

      case "Phone":
          contact.PhoneContactPreference_Ext.isFieldChanged("Opt") {
        contact.PhoneContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
        contact.PhoneContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_INTERNAL
      }
          break
      case "PaperMail":
          contact.PaperMailContactPreference_Ext.isFieldChanged("Opt") {
        contact.PaperMailContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
        contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_INTERNAL
      }
          break
    }
  }


  /**
   * Sets the default value for the 'Opt' and 'Last Source of Update' fields of digital, phone or paper mail contact preferences.
   * 'opt' option.
   */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Returns("a value to indicate the method has been called")
  static function setDefaultContactPreferenceValues(contact:ABContact):boolean {

    if (contact.DigitalContactPreference_Ext.Opt == null) {
      contact.DigitalContactPreference_Ext.Opt = typekey.ABContactPreferOption_Ext.TC_OPTOUT
    }
    if(contact.PhoneContactPreference_Ext.Opt == null) {
      contact.PhoneContactPreference_Ext.Opt = typekey.ABContactPreferOption_Ext.TC_OPTIN
    }
    if(contact.PaperMailContactPreference_Ext.Opt == null) {
      contact.PaperMailContactPreference_Ext.Opt = typekey.ABContactPreferOption_Ext.TC_OPTIN
    }

    if (contact.DigitalContactPreference_Ext.LastSourceOfUpdate == null){
      contact.DigitalContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_DEFAULT
    }

    if (contact.PhoneContactPreference_Ext.LastSourceOfUpdate == null){
      contact.PhoneContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_DEFAULT
    }
    if (contact.PaperMailContactPreference_Ext.LastSourceOfUpdate == null){
      contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_DEFAULT
    }

    return true
  }
}
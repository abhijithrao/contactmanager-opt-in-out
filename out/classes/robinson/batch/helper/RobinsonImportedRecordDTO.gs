package robinson.batch.helper

uses java.util.Date
uses java.text.SimpleDateFormat

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 5/1/16
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
class RobinsonImportedRecordDTO {
  var IndexID: String as IndexID
  var LastName: String as LastName
  var FirstName: String as FirstName
  var Language: String as Language
  var SexCode: String as SexCode
  var Title: String as Title
  var Company: String as Company
  var Street: String as Street
  var HouseNumber: String as HouseNumber
  var BoxNumber: String as BoxNumber
  var Address1: String as Address1
  var PostalCode: String as PostalCode
  var Locality: String as Locality
  var FunctionCode: String as FunctionCode
  var OriginCode: String as OriginCode
  var IDUpdate: String as IDUpdate
  var RegistrationDate: Date as RegistrationDate
  var ConfirmationCode: String as ConfirmationCode
  var ConfirmationDate: Date as ConfirmationDate
  var RecordNumber: String as RecordNumber
  var gwRobinsonInboundRec_Ext: RobinsonInboundRec_Ext as GWRobinsonInboundRec_Ext
  construct(record: RobinsonInboundRec_Ext) {
    IndexID = record.IndexID
    LastName = record.LastName
    FirstName = record.FirstName
    Language = record.Language
    SexCode = record.SexCode
    Title = record.Title
    Company = record.Company
    Street = record.Street
    HouseNumber = record.HouseNumber
    BoxNumber = record.BoxNumber
    Address1 = record.Address1
    PostalCode = record.PostalCode
    Locality = record.Locality
    FunctionCode = record.FunctionCode
    OriginCode = record.OriginCode
    IDUpdate = record.IDUpdate
    RegistrationDate = record.RegistrationDate != null ? getDateFromddMMyyFormat(record.RegistrationDate) : null
    ConfirmationCode = record.ConfirmationCode
    ConfirmationDate = record.ConfirmationDate != null ? getDateFromddMMyyFormat(record.ConfirmationDate) : null
    RecordNumber = record.RecordNumber

    gwRobinsonInboundRec_Ext = record
  }

  function getDateFromddMMyyFormat(dateString: String): Date {
    var df = new SimpleDateFormat("dd/MM/yy")
    var convertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(convertCurrentDate)
    return date
  }
}
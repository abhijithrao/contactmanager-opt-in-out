package robinson.batch

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/29/16
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
uses gw.api.util.ConfigAccess
uses gw.processes.BatchProcessBase
uses java.io.File
uses java.lang.Exception
uses java.io.FileReader
uses java.io.BufferedReader
uses java.text.SimpleDateFormat
uses java.util.Date
uses java.lang.System
uses gw.api.system.PLConfigParameters
uses java.lang.StringBuilder
uses java.util.ArrayList
uses java.lang.StringIndexOutOfBoundsException
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses util.IntegrationPropertiesUtil
uses gw.api.database.Relop
uses gw.api.database.Query
uses org.apache.commons.lang.StringUtils
uses util.FilesUtil
uses util.common.CommonConstants
uses robinson.dto.RobinsonHelperUtil
uses robinson.batch.helper.RobinsonImportedRecordDTO

class RobinsonImportBatch extends BatchProcessBase {
  override function checkInitialConditions(): boolean {
    return true
  }

  override function requestTermination(): boolean {
    return true
  }
  private static final var LOGGER: Logger = LoggerFactory.getLogger(RobinsonImportBatch)
  private static final var file_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_file_name")
  private static final var source_File_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_File_path")
  private static final var targetFile = IntegrationPropertiesUtil.getProperty("RobinsonInbound_targetFile")
  private static final var client_key = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_key")
  private static final var client_separator = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_separator")
  private static final var daily_folder_pattern = IntegrationPropertiesUtil.getProperty("RobinsonInbound_file_daily_folder_pattern")
  private static final var archive_pass_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_pass_file_path")
  private static final var archive_fail_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_fail_file_path")
  private static final var ftp_archive_pass_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_FTPArchive_File_path")
  private static final var ftp_archive_fail_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_FTPFailed_File_path")
  private static final var date_time_format = IntegrationPropertiesUtil.getProperty("RobinsonInbound_date_time_format")
  private static final var client_smb = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_smb")
  private static final var remote_domain_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_domain_name")
  private static final var archive_user_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_user_name")
  private static final var archive_password_value = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_password_value")
  private static final var remote_user_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_user_name")
  private static final var remote_password_value = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_password_value")
  // private static final var file_directory = IntegrationPropertiesUtil.getProperty("RobinsonInbound_dir_locatiin")

  construct() {
    super(BatchProcessType.TC_ROBINSONSIMPORTBATCH)
  }

  /**
   *   Batch Process Method for  Robinsons INBOUND BATCH
   */
  override function doWork() {
    LOGGER.info("RobinsonImportBatch :doWork : Begins ")
    try {
      cleanTable()
      processSourceFile(file_name, source_File_path, targetFile)

      processRecords()
    } catch (e: Exception) {
      LOGGER.error(" Exception in RobinsonImportBatch :doWork  method : " + e)
    }
    LOGGER.info("RobinsonImportBatch :doWork : Ends ")
  }

  /**
   *  Method reads flat file and update DCNMStatusInbound_Ext entity
   */
  private function updateDatabase(file: File): int {
    LOGGER.info("RobinsonImportBatch : updateDCNMStatusToDatabase : Begins ")
    var br: BufferedReader = null
    var errorStatus: int = 0
    try {
      br = new BufferedReader(new FileReader(file))
      var line = br.readLine()
      while (line != null && line != "") {
        //skipping first line of flat file as it contains header information
        // line = br?.readLine()
        if (line.NotBlank)   {

          var IndexID = getRobinsonImportColumn(line, 0, 15)
          var LastName = getRobinsonImportColumn(line, 15, 50)
          var FirstName = getRobinsonImportColumn(line, 50, 65)
          var Language = getRobinsonImportColumn(line, 65, 66)
          var SexCode = getRobinsonImportColumn(line, 66, 68)
          var Title = getRobinsonImportColumn(line, 68, 98)
          var Company = getRobinsonImportColumn(line, 98, 138)
          var Street = getRobinsonImportColumn(line, 138, 178)
          var HouseNumber = getRobinsonImportColumn(line, 178, 182)
          var BoxNumber = getRobinsonImportColumn(line, 182, 186)
          var Address1 = getRobinsonImportColumn(line, 186, 216)
          var PostalCode = getRobinsonImportColumn(line, 216, 220)
          var Locality = getRobinsonImportColumn(line, 220, 252)
          var FunctionCode = getRobinsonImportColumn(line, 252, 255)
          var OriginCode = getRobinsonImportColumn(line, 255, 257)
          var IDUpdate = getRobinsonImportColumn(line, 257, 263)
          var RegistrationDate = getRobinsonImportColumn(line, 263, 271)
          var ConfirmationCode = getRobinsonImportColumn(line, 271, 272)
          var ConfirmationDate = getRobinsonImportColumn(line, 272, 280)
          var RecordNumber = getRobinsonImportColumn(line, 280, 286)


          LOGGER.debug("=================================Inserting Data Into the staging Table=====The Records len is :" + line.length)
          LOGGER.info("IndexID :" + IndexID +
              "\tLastName :" + LastName +
              "\tFirstName :" + FirstName +
              "\tLanguage :" + Language +
              "\tSexCode :" + SexCode +
              "\tTitle :" + Title +
              "\tCompany :" + Company +
              "\tStreet :" + Street +
              "\tHouseNumber:" + HouseNumber +
              "\tBoxNumber :" + BoxNumber +
              "\tAddress1 :" + Address1 +
              "\tPostalCode :" + PostalCode +
              "\tLocality:" + Locality +
              "\tFunctionCode :" + FunctionCode +
              "\tOriginCode  :" + OriginCode +
              "\tIDUpdate :" + IDUpdate +
              "\tRegistrationDate :" + RegistrationDate +
              "\tConfirmationCode :" + ConfirmationCode +
              "\tConfirmationDate :" + ConfirmationDate +
              "\tRecordNumber :" + RecordNumber)
            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
                LOGGER.info("##################### Adding New Record into Staging RobinsonInboundRec_Ext Table")
                var robinsonInboundRecord = new RobinsonInboundRec_Ext()
                bundle.add(robinsonInboundRecord)

                robinsonInboundRecord.IndexID = getRobinsonImportColumn(line, 0, 15)
                robinsonInboundRecord.LastName = getRobinsonImportColumn(line, 15, 50)
                robinsonInboundRecord.FirstName = getRobinsonImportColumn(line, 50, 65)
                robinsonInboundRecord.Language = getRobinsonImportColumn(line, 65, 66)
                robinsonInboundRecord.SexCode = getRobinsonImportColumn(line, 66, 68)
                robinsonInboundRecord.Title = getRobinsonImportColumn(line, 68, 98)
                robinsonInboundRecord.Company = getRobinsonImportColumn(line, 98, 138)
                robinsonInboundRecord.Street = getRobinsonImportColumn(line, 138, 178)
                robinsonInboundRecord.HouseNumber = getRobinsonImportColumn(line, 178, 182)
                robinsonInboundRecord.BoxNumber = getRobinsonImportColumn(line, 182, 186)
                robinsonInboundRecord.Address1 = getRobinsonImportColumn(line, 186, 216)
                robinsonInboundRecord.PostalCode = getRobinsonImportColumn(line, 216, 220)
                robinsonInboundRecord.Locality = getRobinsonImportColumn(line, 220, 252)
                robinsonInboundRecord.FunctionCode = getRobinsonImportColumn(line, 252, 255)
                robinsonInboundRecord.OriginCode = getRobinsonImportColumn(line, 255, 257)
                robinsonInboundRecord.IDUpdate = getRobinsonImportColumn(line, 257, 263)
                robinsonInboundRecord.RegistrationDate = getRobinsonImportColumn(line, 263, 271)
                robinsonInboundRecord.ConfirmationCode = getRobinsonImportColumn(line, 271, 272)
                robinsonInboundRecord.ConfirmationDate = getRobinsonImportColumn(line, 272, 280)
                robinsonInboundRecord.RecordNumber = getRobinsonImportColumn(line, 280, 286)
          }, "su")
        }
        line = br?.readLine()
      }
    }
        catch (ex: Exception) {
          handleRobinsonInboundException(ex, ex.Message)
          LOGGER.error("Error while parsing and saving the flat file data into Robinsons Staging Table : " + "\\n" + ex.StackTraceAsString)
          errorStatus = - 1
        }
        finally {
      if (br != null) {
        br.close()
      }
    }
    LOGGER.info("RobinsonImportBatch : updateDatabase : Ends ")
    return errorStatus
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleImportException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   *     Method load file frm remote location and copy it to local location
   *     and updated file content to database
   */
  private function processSourceFile(fileName: String, sourceFilePath: String, targetFilePath: String) {
    LOGGER.info("RobinsonImportBatch : processSourceFile : Begins ")
    var errorCount = 0
    try {
      var file: File = null
      try {
        LOGGER.info("RobinsonImportBatch : fileName : " + fileName + ", sourceFilePath :" + sourceFilePath + ", targetFilePath :" + targetFilePath)
        // Build source path
        var sourcePath = sourceFilePath + fileName
        var targetPath = targetFilePath + fileName

        LOGGER.debug("RobinsonImportBatch : sourcePath : " + sourcePath)
        LOGGER.debug("RobinsonImportBatch : targetPath : " + targetPath)
        LOGGER.debug("RobinsonImportBatch : env : " + java.lang.System.getProperty("gw.pc.env"))

        util.FilesUtil.nioCopyFromSourceToDestination(new File(sourcePath), new File(targetPath))

        file = new File(targetPath)

        if (file.canRead()) {
          LOGGER.info(" RobinsonImportBatch  file can read")
          try {
            try {
              // Update database with generated flat file
              errorCount = updateDatabase(file)
              LOGGER.debug("RobinsonImportBatch : Add Record to DCNMStaging Entity :errorCount " + errorCount)
              //Operation Completed Successfully
              incrementOperationsCompleted()
            } catch (ex: Exception) {
              handleRobinsonInboundException(ex, "Could NOT update database with file  " + file.Name)
            }
          } catch (ex: Exception) {
            handleRobinsonInboundException(ex, "Exception on processSourceFile() - Could NOT generate flat file  " + file.Name)
          }
          try {
            if (errorCount < 0) {
              moveFilesDependingOnStatus(CommonConstants.FTP_FAILED)
            } else {
              moveFilesDependingOnStatus(CommonConstants.FTP_PROCESSED)
            }
          } catch (ex: Exception) {
            handleRobinsonInboundException(ex, "Exception in deleting file " + file.AbsolutePath)
          }
        } else {
          var message = "Error in Moving file " + file.AbsolutePath + "/"
        }
      }
          catch (ex: Exception) {
            handleRobinsonInboundException(ex, " Exception duringcopyFile - For file  " + fileName)
          }
    } catch (ex: Exception) {
      handleRobinsonInboundException(ex, "Exception on processSourceFile() - Could NOT process file " + fileName)
    }
    LOGGER.info("RobinsonImportBatch : processSourceFile : Ends ")
  }

  /**
   *   converts String to Date
   */
  private static  function convertStringToDate(dateString: String): Date {
    var formatter = new SimpleDateFormat("yyyyMMdd")
    if (dateString != null) {
      return (formatter.parse(dateString) )
    }
    else
      return null
  }

  //Query the CM Contact from here

  private function processRecords() {
    var errorCount = 0
    LOGGER.info("Entering Method processRecords()")
    var foundContacts = gw.api.database.Query.make(ABContact).select()

    try {
      foundContacts.each(\contact -> {
        LOGGER.debug("processRecords() , Searching for Contact :" + contact)
        var matchingRecords = returnRobinsonRecordsMatchingPostCodeAndName(contact)

        LOGGER.debug("Found Any Contact Matching from Robinsons Mail List ?? for GWContact :Count :" + matchingRecords?.Count)

        matchingRecords.each(\matchingRecord -> {
          LOGGER.debug("Iterating New MatchingRecord...")
          var robinsonUtil = new RobinsonHelperUtil(contact, matchingRecord)
          var dto = robinsonUtil.getRobinsonDTO()
          if (dto != null &&
              robinsonUtil.countOfCriteriaMatched() >= 5){
            LOGGER.info("********* A matching Record was Found for Contact,Updating the preference Now : " + contact)
            updateRobinsonPreferenceOnContact(contact, dto)
            LOGGER.debug("*************Continuing with Next Record if any..")
          }
        })
      })
    } catch (e: Exception) {
      errorCount = 1
      throw new Exception("Record Processing Failed RobinsonImportBatch.processSourceRecord()=" + e.Message)
    }
        finally {
      if (errorCount < 0) {
        moveFilesDependingOnStatus(CommonConstants.FAILED)
      } else {
        moveFilesDependingOnStatus(CommonConstants.SUCCESS)
      }
      LOGGER.info("Leaving Method processRecords()")
    }
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleRobinsonInboundException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  function cleanTable() {
    var robinsonsInboundRecords_Ext = gw.api.database.Query.make(RobinsonInboundRec_Ext).select()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      robinsonsInboundRecords_Ext.each(\elt -> {
        bundle.add(elt)
        bundle.delete(elt)
      })
    }, PLConfigParameters.UnrestrictedUserName.Value)
  }

  function getDate(dateString: String): Date {
    var df = new SimpleDateFormat("yyyyMMdd")
    var cnvertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(cnvertCurrentDate)
    return date
  }

  function getDateTimeStringForFile(dateTimeFormat: String): String {
    var dateFormat = new SimpleDateFormat(dateTimeFormat)
    var date = new java.util.Date()
    return dateFormat.format(date)
  }

  function addTimestamp(name: String, timePart: String): String {
    var lastIndexOf: int = name.lastIndexOf('.')
    return (lastIndexOf == - 1 ? name + "_" + timePart : name.substring(0, lastIndexOf) + "_" + timePart + name.substring(lastIndexOf)).replaceAll("[\\/:\\*\\?\"<>| ]", "_")
  }

  function updateContactPreferences(contact: ABContact, optOutDate: Date): boolean {
    LOGGER.info("In Method.updateContactPreferences()...")

    if ((contact.PaperMailContactPreference_Ext != null ) &&
        (contact.PaperMailContactPreference_Ext.LastSourceOfUpdate == null || contact.PaperMailContactPreference_Ext.LastSourceOfUpdate != ABLastSourceOfUpdate_Ext.TC_INTERNAL) &&
        ( (contact.PaperMailContactPreference_Ext.LatestUpdateDateTime?.differenceInDays(optOutDate) > 0 )
            || (contact.PaperMailContactPreference_Ext.LatestUpdateDateTime == null))) {

      gw.transaction.Transaction.runWithNewBundle(\b -> {
        b.add(contact)
        contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = ABLastSourceOfUpdate_Ext.TC_ROBINSON
        contact.PaperMailContactPreference_Ext.LatestUpdateDateTime = optOutDate
        contact.PaperMailContactPreference_Ext.Opt = ABContactPreferOption_Ext.TC_OPTOUT
      }, "su")
      LOGGER.info(" updateContactPreferences() Successfully Updated the contact:" + contact)
      return true
    } else {
      LOGGER.info("Debug Value : Did not find the matching contact skipping the current :" + contact.DisplayName)
      return false
    }
  }

  private function moveFilesDependingOnStatus(fileStatus: String) {
    if (util.common.CommonConstants.SUCCESS.equalsIgnoreCase(fileStatus)) {

      var localFile: File = new File (targetFile + file_name)
      if (localFile.exists()) {
        var archievedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
        var archievedFilePath = archive_pass_file_path + File.separator + archievedFileName;
        FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
        localFile.delete()
      }
      else {
        LOGGER.info("Local Source File Not Found")
      }
    }
    else if (util.common.CommonConstants.FTP_PROCESSED.equalsIgnoreCase(fileStatus)) {
      var localFile: File = new File (source_File_path + file_name)
      if (localFile.exists()) {
        var archievedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
        var archievedFilePath = ftp_archive_pass_file_path + File.separator + archievedFileName;
        FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
        localFile.delete()
      }
      else {
        LOGGER.info("Local Source File Not Found")
      }
    }
    else if (util.common.CommonConstants.FTP_FAILED.equalsIgnoreCase(fileStatus)) {
        var localFile: File = new File (source_File_path + file_name)
        if (localFile.exists()) {
          var archievedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
          var archievedFilePath = ftp_archive_fail_file_path + File.separator + archievedFileName;
          FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
          localFile.delete()
        }
        else {
          LOGGER.info("Local Source File Not Found")
        }
      }
  }

  private function getRobinsonImportColumn(line: String, startIndex: int, endIndex: int): String {
    var columnValue: String = " "
    if (line != null and line != "")     {
      columnValue = line.substring(startIndex, endIndex)
    }
    if (columnValue != null){
      columnValue = StringUtils.deleteWhitespace(columnValue)
    }
    return columnValue
  }

  private function returnRobinsonRecordsMatchingPostCodeAndName(contact: ABContact): List<RobinsonInboundRec_Ext> {
    LOGGER.debug("Type of Contact : " + contact.IntrinsicType)
    LOGGER.debug("PostCode of the Contact :" + contact.PrimaryAddress.PostalCode)
    if (contact typeis ABPerson){

      var robinsonsInboundQuery = gw.api.database.Query.make(RobinsonInboundRec_Ext)
          .compareIgnoreCase(RobinsonInboundRec_Ext#PostalCode, Relop.Equals, contact?.PrimaryAddress?.PostalCode)
      /* robinsonsInboundQuery.or(\ orCriteria -> {
              orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#FirstName,Relop.Equals,contact.FirstName)
              orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#LastName,Relop.Equals,contact.LastName)
      })*/
      /* robinsonsInboundQuery.and(\ andCriteria -> {
         andCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#PostalCode,Relop.Equals,contact?.PrimaryAddress?.PostalCode)
         andCriteria.or (\orCriteria -> {
         orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#FirstName,Relop.Equals,contact.FirstName)
         orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#LastName,Relop.Equals,contact.LastName)
         })
       })
       robinsonsInboundQuery.and(\ andCriteria ->{

       })*/
      var results = robinsonsInboundQuery.select()
      var filteredResult = results.where(\elt -> elt.PostalCode.equalsIgnoreCase(contact?.PrimaryAddress?.PostalCode))
      LOGGER.debug("****************** Matches Found in Result After Filtering filteredResult:" + filteredResult.Count)
      return filteredResult?.toList()
    }
    return null
  }

  private function updateRobinsonPreferenceOnContact(contact: ABContact, robinsonDTO: RobinsonImportedRecordDTO) {
    LOGGER.info("Entering Method : updateRobinsonPreferenceOnContact()...")
    if (updateContactPreferences(contact, robinsonDTO.ConfirmationDate)) {

      var record = robinsonDTO.GWRobinsonInboundRec_Ext
      gw.transaction.Transaction.runWithNewBundle(\b -> {
        try {
          b.add(record)
          //  processDCNMRejectionStatus(record)
        } catch (ex: Exception) {
          // record.ProcessedMessage = ex.LocalizedMessage
          LOGGER.error(ex)
          //TO DO - TO BE REMOVED
          //throw new Exception("Record Processing Failed DCNMBatch.callDCNMFailedAction()=" + record.Processed)
          record.Processed = Boolean.FALSE
        } finally {
          record.Processed = Boolean.TRUE
        }
      }, "su")
    }
    else {
      LOGGER.info(contact + " -Contact could not be updated..")
    }
  }
}
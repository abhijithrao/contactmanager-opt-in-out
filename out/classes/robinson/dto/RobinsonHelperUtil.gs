package robinson.dto

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 5/1/16
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses robinson.batch.helper.RobinsonImportedRecordDTO

class RobinsonHelperUtil {


  private static final var LOGGER: Logger = LoggerFactory.getLogger(RobinsonHelperUtil)
  public var robinsonRecordDTO: RobinsonImportedRecordDTO
  var gwContact: entity.ABContact
  var areAllCriteriaMatchFound: boolean = false
  var isPostCodeMatchFound: boolean = false
  var isHouseNumberFound: boolean = false
  var isLastNameMatchFound: boolean = false
  var isFirstNameMatchFound: boolean = false
  var isCityMatchFound: boolean = false
  var isStreetMatchFound: boolean = false
  var isCountryMatchFound: boolean = false
  construct(contact: ABContact, record: RobinsonInboundRec_Ext) {
    robinsonRecordDTO = new RobinsonImportedRecordDTO(record)
    gwContact = contact
  }

  function getRobinsonDTO(record: RobinsonInboundRec_Ext): RobinsonImportedRecordDTO {
    robinsonRecordDTO = new RobinsonImportedRecordDTO(record)
    return robinsonRecordDTO
  }

  public function getRobinsonDTO(): RobinsonImportedRecordDTO {
    if (robinsonRecordDTO != null){
      return robinsonRecordDTO
    }
    return null
  }

  function validateAndGetCount() {
    if (robinsonRecordDTO != null && gwContact != null){

      if (gwContact typeis ABPerson)  {

        isPostCodeMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.PostalCode, robinsonRecordDTO.PostalCode)
        isHouseNumberFound = findMatchesInAddress(gwContact?.PrimaryAddress.AddressLine1, robinsonRecordDTO.HouseNumber)
        isLastNameMatchFound = findMatchesInAddress(gwContact?.FirstName, robinsonRecordDTO.LastName)
        isFirstNameMatchFound = findMatchesInAddress(gwContact?.LastName, robinsonRecordDTO.FirstName)
        isCityMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.City, robinsonRecordDTO.Locality)
        isStreetMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress.AddressLine3, robinsonRecordDTO.Street)
        isCountryMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.Country, Country.TC_BE.Code)
      }
      LOGGER.info("\n isPostCodeMatchFound =" + isPostCodeMatchFound +
          "\tisHouseNumberFound = " + isHouseNumberFound +
          "\tisLastNameMatchFound =" + isLastNameMatchFound +
          "\tisFirstNameMatchFound =" + isFirstNameMatchFound +
          "\tisCityMatchFound =" + isCityMatchFound +
          "\tisStreetMatchFound =" + isStreetMatchFound +
          "\tisCountryMatchFound =" + isCountryMatchFound)
    }
  }

  function countOfCriteriaMatched(): int {
    validateAndGetCount()
    var count = 0

    if (isPostCodeMatchFound) count++
    if (isHouseNumberFound)  count++
    if (isLastNameMatchFound)  count++
    if (isFirstNameMatchFound) count++
    if (isCityMatchFound)  count++
    if (isStreetMatchFound)  count++
    if (isCountryMatchFound)  count++
    LOGGER.info(" TOTAL Matches on Criteria found" + count)
    return count
  }

  function countOfCompulsoryCriteriaMatched(): int {
    validateAndGetCount()
    var count = 0

    if (isPostCodeMatchFound) count++
    if (isHouseNumberFound)  count++
    if (isLastNameMatchFound || isFirstNameMatchFound)  count++
    if (isCityMatchFound)  count++
    if (isStreetMatchFound)  count++
    if (isCountryMatchFound)  count++
    return count
  }

  function findMatchesInAddress(primaryAddress: String, portionToMatch: String): boolean {
    if (primaryAddress != null && portionToMatch != null){

      return primaryAddress.containsIgnoreCase(portionToMatch)
    }
    return false
  }
}
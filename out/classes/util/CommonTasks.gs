package util

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/21/16
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
uses java.lang. *
uses java.io.File
uses java.io.IOException
uses java.nio.file.Files
uses java.nio.file.Path
uses java.nio.file.Paths
uses javax.xml.bind.JAXBException
uses org.apache.commons.io.FileUtils
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses org.faceless.util.log.PropertyReader
uses util.common.CommonConstants
uses java.net.URI
uses java.nio.file.LinkOption

public class CommonTasks {
  private static final var LOGGER: Logger = LoggerFactory.getLogger(CommonTasks)
  public static var SourceBaseDirectory: String
  public static var DeliveredBaseDirectory: String
  public static var InflightBaseDirectory: String
  public static var FailedBaseDirectory: String
  public static var ArchivedBaseDirectory: String
  public construct() {
    setBaseDirectoryPaths()
  }

  public static function determineDestinationPath(source: Path, deliveryState: String): Path {
    var destination: Path = null
    if (deliveryState.equalsIgnoreCase(CommonConstants.SUCCESS)) {
      LOGGER.debug("SourceBaseDirectory :" + SourceBaseDirectory)
      LOGGER.debug("ArchivedBaseDirectory :" + ArchivedBaseDirectory)
      var destfilePath: String = source.toString().replace(SourceBaseDirectory, ArchivedBaseDirectory)
       var f = new File(destfilePath)
      destination = Paths.get( f.toURI())
      LOGGER.info("Delivered file path is" + destination.toString())
      return destination
    }
    else if (deliveryState.equalsIgnoreCase(CommonConstants.FAILED)) {
      LOGGER.debug("SourceBaseDirectory :" + SourceBaseDirectory)
      LOGGER.debug("FailedBaseDirectory : " + FailedBaseDirectory)
      var destfilePath: String = source.toString().replace(SourceBaseDirectory, FailedBaseDirectory)
      var f = new File(destfilePath)
      destination = Paths.get( f.toURI())
      LOGGER.info("Inflight file path is" + destination.toString())
      return destination
    } else {
        return destination

    }
  }

  public static function setBaseDirectoryPaths(): void {
    LOGGER.info("=============setBaseDirectoryPaths()==========")
    
    SourceBaseDirectory = IntegrationPropertiesUtil.getProperty("root.source.directory")
    DeliveredBaseDirectory = IntegrationPropertiesUtil.getProperty("root.destination.delivered.directory")
    InflightBaseDirectory = IntegrationPropertiesUtil.getProperty("root.destination.inflight.directory")
    ArchivedBaseDirectory = IntegrationPropertiesUtil.getProperty("root.archieved.directory")
    LOGGER.debug(SourceBaseDirectory)
    LOGGER.debug(DeliveredBaseDirectory)
    LOGGER.debug(InflightBaseDirectory)
    LOGGER.debug(ArchivedBaseDirectory)
  }
    public static function setBaseDirectoryPaths(sourceDirectory : String,failedDirectory : String, archivedDirectory : String): void {
    LOGGER.info("=============setBaseDirectoryPathsWithParams()==========")

    SourceBaseDirectory =   sourceDirectory
    FailedBaseDirectory = failedDirectory
    ArchivedBaseDirectory = archivedDirectory
    LOGGER.debug(SourceBaseDirectory)
    LOGGER.debug(DeliveredBaseDirectory)
    LOGGER.debug(ArchivedBaseDirectory)
  }

  @SuppressWarnings("unused")
  public static function moveFileTo(source: Path, deliveryState: String): boolean {
    LOGGER.info("Source File :{}", source.toString())
    LOGGER.info("File Being moved to :{}", deliveryState)
    var destination: Path = determineDestinationPath(source, deliveryState)

    LOGGER.info("Destination path determined is : " + destination.toString())
    var successFullyMoved = false
    if (destination == null) {
      LOGGER.error("Could not determine the destination path, Please check the Properties file/or any issues with CaseSensitivity issues")
      return false
    }
    LOGGER.info("The File will be moved to : " + destination.toString() + ": DeliveryState is :" + deliveryState)
    if (!Files.exists(destination.getParent(), new LinkOption[]{LinkOption.NOFOLLOW_LINKS})) {
      if (destination.toFile().getParentFile().exists() or destination.toFile().getParentFile().mkdirs()) {
        LOGGER.info("Directory structure at destination did not exist,hence created the path now...")
      }
    }
    if (source != null and destination != null) {
      if (Files.exists(destination.getParent(), new LinkOption[]{LinkOption.NOFOLLOW_LINKS}) and !destination.toFile().exists() and Files.isDirectory(destination.getParent(), new LinkOption[]{LinkOption.NOFOLLOW_LINKS})) {
        LOGGER.info("Parent Path Exists, its writable and is a directory..File will be force copied now")

        try {
          FilesUtil.nioCopyFromSourceToDestination(source.toFile().getCanonicalFile(), destination.toFile().getCanonicalFile())
          successFullyMoved = true
        }
            catch (e: IOException) {
              LOGGER.error("File moved , or not available")
              return false
            }
      } else {
        LOGGER.error("File Cannot be moved to :{} as destination File Already Exists :{}", deliveryState, destination)
        return false
      }
      return (successFullyMoved and destination.toFile().exists())
    }
    LOGGER.error("moveFileTo() - Failed to move file " + source)
    return false
  }


  public static function getContentAsString(fileName: String): String {
    var content: String = null

    try {
      content = FileUtils.readFileToString(new File(fileName))
    }
        catch (e: IOException) {
          e.printStackTrace()
          return null
        }

    return content
  }
}
